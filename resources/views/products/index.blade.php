@extends('layout.app')
@section('content')

        @if($message=Session::get('success'))
        <div class='alert alert-primary'>
            {{$message}}
        </div>
        @endif

        @if($message=Session::get('error'))
        <div class='alert alert-danger'>
            {{$message}}
        </div>
        @endif
        <a href="{{route('products.create')}}" class="btn btn-primary">Create</a>
        
            <div class='mt-4'>
                    <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">name</th>
                                <th scope="col">price</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $i=>$product)
                                    <tr>
                                    <th scope="row">{{$i+1}}</th>
                                        <td>{{$product->name}}</td>
                                        <td>{{$product->price}}</td>
                                        <td>
                                            <div class='row'>
                                                <div class='col'>
                                                <a href="{{route('products.edit',$product->id)}}" class='btn btn-success'>Edit</a>
                                                </div>
                                                <div class='col'>
                                                        <a href="{{route('products.show',$product->id)}}"class='btn btn-primary'>Show</a>
                                                </div>
                                                <div class='col'>

                                                <form action="{{route('products.destroy',$product->id)}}" method='POST'>
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type='submit'class='btn btn-danger'>Delete</button>
                                                </form>
                                                       
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
        </div>


@endsection