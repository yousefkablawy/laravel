@extends('layout.app')
@section('content')
<form action="{{route('products.store')}}" method="POST">
    @csrf
    <div class="mb-3">
      <label class="form-label">product name</label>
      <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="enter a product name" >
      @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    </div>
    <div class="mb-3">
        <label class="form-label">product price</label>
        <input type="text" name="price" class="form-control  @error('price') is-invalid @enderror" placeholder="enter a product price">
        @error('price')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    </div>
    <div class="mb-3">
        <label  class="form-label">product detail</label>
        <textarea name='detail' class="form-control @error('detail') is-invalid @enderror "  rows="3"></textarea>
        @error('detail')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection