@extends('layout.app')
@section('content')
    <div class='m-4'>
        <a href="{{route('products.index')}}" class='btn btn-primary my-2'>Back to home page</a>
        <div class='card'>
            <h2 class='card-title mx-2'>{{$product->name}}</h2>
            <p class='card-body'>{{$product->detail}}</p>
            <span class='card-footer'>{{$product->price}} - craeted at {{$product->created_at}}</span>
        </div>
    </div>
   
@endsection