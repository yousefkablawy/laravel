@extends('layout.app')
@section('content')
<a href="{{route('products.index')}}" class='btn btn-primary my-2'>Back to home page</a>
<form action="{{route('products.update',$product->id)}}" method="POST">
        @csrf
        @method('PUT')
        <div class="mb-3">
          <label class="form-label">product name</label>
          <input type="text" name="name" value={{$product->name}} class="form-control  @error('name') is-invalid @enderror" placeholder="enter a product name" >
          @error('name')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
        </div>
        
        <div class="mb-3">
            <label class="form-label">product price</label>
            <input type="text" name="price" value={{$product->price}} class="form-control @error('price') is-invalid @enderror" placeholder="enter a product price">
            @error('price')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        </div>
        <div class="mb-3">
            <label  class="form-label">product detail</label>
            <textarea name='detail' class="form-control  @error('detail') is-invalid @enderror"  rows="3">{!! $product->detail !!}</textarea>
            @error('detail')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
@endsection