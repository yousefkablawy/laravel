<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

/*
    Haidar  add the following comments:
        - I prefer to use with() instead of compact(). --> compact is deprecated!
        - validate with different rules like string, nullable, between and file whatever.
        - there is a bug when you used create() and update()... you passed request->all() and this means all the data
             in the request will be passed not the validated ones... so use request->validated().
        - when you delete a resource make the message name error, I think this the best naming convention! Don't confuse about this comment, we can discuss it later.
        - also, in delete function, you pass the Product instance... Please explain what the idea! I think something relate to bug.
*/



class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products=Product::all();
        return view('products.index')->with('products',$products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatData=$request->validate([
            'name'=>'required|max:100',
            'price'=>'required|numeric',
            'detail'=>'required|max:255',
        ]);
        $product=Product::create($validatData);
        return redirect()->route('products.index')
        ->with('success','product added successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
        return view('products.show')->with('product',$product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
        return view('products.edit')->with('product',$product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
        $validatData=$request->validate([
            'name'=>'required|max:100',
            'price'=>'required|numeric',
            'detail'=>'required|max:255',
        ]);
        $product->update($validatData);
        return redirect()->route('products.index')
        ->with('success','product updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
        $product->delete();
        return redirect()->route('products.index')
        ->with('error','product deleted successfully');

    }
}
